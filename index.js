const report = require('multiple-cucumber-html-reporter');

report.generate({
    jsonDir: './test/report/',
    reportPath: './test/report/',
    openReportInBrowser: true,
    metadata:{
        browser: {
            name: 'chrome',
            version: '88.0.4324.96'
        },
        device: 'Local test machine',
        platform: {
            name: 'Fabians MacBook Pro',
            version: 'macOS 11.1'
        }
    },
    customData: {
        title: 'Run info',
        data: [
            {label: 'Project', value: 'Custom project'},
            {label: 'Release', value: '1.2.3'},
            {label: 'Cycle', value: 'B11221.34321'},
            {label: 'Execution Start Time', value: 'Jan 27th 2021, 10:31 PM EST'},
            {label: 'Execution End Time', value: 'Jan 27th 2021, 10:32 PM EST'}
        ]
    }
});