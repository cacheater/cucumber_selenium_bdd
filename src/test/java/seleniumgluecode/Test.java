package seleniumgluecode;

import com.sun.source.tree.AssertTree;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pom.CarPage;
import pom.ProductDetailsPage;

public class Test extends TestBase{

    @Given("^El cliente se encuentra en la pagina amazon.com.mx$")
    public void elClienteSeEncuentraEnLaPaginaAmazonComMx() throws Throwable {
        Assert.assertTrue(homePage.homePageIsDisplayed());
    }

    @When("^El cliente realiza una busqueda de un producto$")
    public void elClienteRealizaUnaBusquedaDeUnProductoProducto() throws Throwable {
        homePage.setTextOnSearchProduct();
        homePage.clickOnSearchEngine();
    }

    @Then("^Se muestran resultados relacionados con la busqueda$")
    public void seMuestranResultadosRelacionadosConLaBusqueda() throws Throwable {
        Assert.assertTrue("No se redirecciono correctamente a la pagina", resultsPage.isTitleResultsAmazon());
    }


    @And("^Selecciono el primer producto de la lista$")
    public void seleccionoElPrimerProductoDeLaLista() throws Throwable  {
        resultsPage.clickOnFirstProduct();
    }

    @And("^Se muestran los detalles del producto$")
    public void seMuestranLosDetallesDelProducto() throws Throwable {
        productDetailsPage.isTitleDetailsProduct();
    }

    @And("^Selecciono agregar al carrito$")
    public void seleccionoAgregarAlCarrito() throws Throwable {
        productDetailsPage.clickOnAddToCart();
    }

    @Then("^Se actualiza el estado de mi carrito$")
    public void seActualizaElEstadoDeMiCarrito() throws Throwable {
        Assert.assertTrue("No se redirecciono correctamente a la pagina", carPage.isAddToCartDisplayed());
    }
}
