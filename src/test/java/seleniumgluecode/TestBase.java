package seleniumgluecode;

import org.openqa.selenium.WebDriver;
import pom.HomePage;
import pom.ResultsPage;
import pom.ProductDetailsPage;
import pom.CarPage;

public class TestBase {

    protected WebDriver driver = Hooks.getDriver();
    protected HomePage homePage = new HomePage(driver);
    protected ResultsPage resultsPage = new ResultsPage(driver);
    protected ProductDetailsPage productDetailsPage = new ProductDetailsPage(driver);
    protected CarPage carPage = new CarPage(driver);

}
