Feature: Compras
  Como cliente de amazon quiero hacer busquedas de prodcutos para agregar al carrito

  Scenario: Agregar productos al carrito
    Given El cliente se encuentra en la pagina amazon.com.mx
    When El cliente realiza una busqueda de un producto
    Then Se muestran resultados relacionados con la busqueda
    And Selecciono el primer producto de la lista
    And Se muestran los detalles del producto
    And Selecciono agregar al carrito
    Then Se actualiza el estado de mi carrito

