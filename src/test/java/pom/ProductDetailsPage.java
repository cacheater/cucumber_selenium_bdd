package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductDetailsPage extends BasePage{

    private String titlePage = "Fitbit Smartwatch Versa 2 con Fitbit Pay - Negro/Carbón: Amazon.com.mx: Salud y Cuidado Personal";
    private By addToCart = By.id("add-to-cart-button");

    private WebDriver driver;
    public ProductDetailsPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isTitleDetailsProduct() throws Exception{
        String expectedPageTitle = driver.getTitle();
        return expectedPageTitle.equalsIgnoreCase(titlePage);
    }

    public void clickOnAddToCart() throws Exception{
        this.click(addToCart);
    }
}
