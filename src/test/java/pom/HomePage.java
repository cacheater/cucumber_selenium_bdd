package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage{

    private String expectedTitleHomePage = "Amazon.com.mx: Precios bajos - Envío rápido - Millones de productos";
    private By tittleAmazon = By.xpath("/html/head/title");
    private By searchProduct = By.id("twotabsearchtextbox");
    private By searchEngine = By.id("nav-search-submit-button");

    public String getExpectedTitleHomePage() {
        return expectedTitleHomePage;
    }
    public By getLittleTesterComics() {
        return tittleAmazon;
    }

    public HomePage(WebDriver driver){
        super(driver);
    }

    public boolean homePageIsDisplayed() throws Exception{
        return this.getTitle().equals(expectedTitleHomePage);
    }

    public void setTextOnSearchProduct() throws Exception{
        this.sendKeys(searchProduct);
    }

    public void clickOnSearchEngine() throws Exception{
        this.click(searchEngine);
    }

}
