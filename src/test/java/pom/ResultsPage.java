package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ResultsPage extends BasePage{

    private By pageTitleLocator = By.xpath("/html/head/title");
    private String titlePage = "Amazon.com.mx : Fitbit Versa 2";
    private By firstProduct = By.xpath("//img[@data-image-latency][1]");

    public By getPageTitleLocator() {
        return pageTitleLocator;
    }
    public String getTitlePage() {
        return titlePage;
    }

    private WebDriver driver;
    public ResultsPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isTitleResultsAmazon() throws Exception{
        String expectedTitle = driver.getTitle();
        return expectedTitle.equalsIgnoreCase(titlePage);
    }

    public void clickOnFirstProduct() throws Exception{
        this.click(firstProduct);
    }

}
