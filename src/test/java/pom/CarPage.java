package pom;

import com.sun.xml.internal.rngom.parse.host.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CarPage extends BasePage {

    private String titlePage = "Carrito de compras de Amazon.com.mx";
    private By addToCartLabel = By.xpath("//h1[contains(text(), 'Agregado al carrito')]");
    String expectedText = "Agregado al carrito";

    private WebDriver driver;
    public CarPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isTitleDetailsProduct() throws Exception{
        String expectedPageTitle = driver.getTitle();
        return expectedPageTitle.equalsIgnoreCase(titlePage);
    }

    public boolean isAddToCartDisplayed() throws Exception{
       return this.getText(addToCartLabel).equals(expectedText);
    }

}
